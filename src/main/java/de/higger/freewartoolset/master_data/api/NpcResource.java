package de.higger.freewartoolset.master_data.api;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.higger.freewartoolset.master_data.domain.Npc;

@RestController
@RequestMapping("/api/npc")
public class NpcResource {

	private static Map<String, Npc> npcs = new HashMap<>();

	@PutMapping
	public void aktualisiereNpc(@RequestBody Npc npc) {
		npcs.put(npc.getLink(), npc);
	}

	@GetMapping("/{bezeichnung}")
	public Optional<Npc> getNpc(String bezeichnung) {
		return npcs.values()
				.stream()
				.filter(w -> w.getBezeichnung()
						.toLowerCase()
						.equals(bezeichnung.toLowerCase()))
				.findAny();
	}

	@GetMapping
	public Collection<Npc> getNpcs() {
		return npcs.values();
	}
}
