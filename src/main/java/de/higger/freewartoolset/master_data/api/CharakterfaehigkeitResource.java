package de.higger.freewartoolset.master_data.api;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.higger.freewartoolset.master_data.domain.Charakterfaehigkeit;

@RestController
@RequestMapping("/api/charakterfaehigkeit")
public class CharakterfaehigkeitResource {

	private static Map<String, Charakterfaehigkeit> charakterfaehigkeiten = new HashMap<>();

	@PutMapping
	public void aktualisiereCharakterfaehigkeit(@RequestBody Charakterfaehigkeit charakterfaehigkeit) {
		charakterfaehigkeiten.put(charakterfaehigkeit.getLink(), charakterfaehigkeit);
	}

	@GetMapping("/{bezeichnung}")
	public Optional<Charakterfaehigkeit> getCharakterfaehigkeit(String bezeichnung) {
		return charakterfaehigkeiten.values()
				.stream()
				.filter(w -> w.getBezeichnung()
						.toLowerCase()
						.equals(bezeichnung.toLowerCase()))
				.findAny();
	}

	@GetMapping
	public Collection<Charakterfaehigkeit> getCharakterfaehigkeiten() {
		return charakterfaehigkeiten.values();
	}
}
