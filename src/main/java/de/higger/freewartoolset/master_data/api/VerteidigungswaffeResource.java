package de.higger.freewartoolset.master_data.api;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.higger.freewartoolset.master_data.domain.Waffe;

@RestController
@RequestMapping("/api/verteidigungswaffe")
public class VerteidigungswaffeResource {

	private static Map<String, Waffe> verteidigungswaffen = new HashMap<>();

	@PutMapping
	public void aktualisiereVerteidigungswaffe(@RequestBody Waffe waffe) {
		verteidigungswaffen.put(waffe.getLink(), waffe);
	}

	@GetMapping("/{bezeichnung}")
	public Optional<Waffe> getVerteidigungswaffe(String bezeichnung) {
		return verteidigungswaffen.values()
				.stream()
				.filter(w -> w.getBezeichnung()
						.toLowerCase()
						.equals(bezeichnung.toLowerCase()))
				.findAny();
	}

	@GetMapping
	public Collection<Waffe> getVerteidigungswaffen() {
		return verteidigungswaffen.values();
	}
}
