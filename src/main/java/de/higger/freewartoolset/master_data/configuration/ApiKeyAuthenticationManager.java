package de.higger.freewartoolset.master_data.configuration;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

public class ApiKeyAuthenticationManager extends Object implements AuthenticationManager {

	private String apikey;

	public ApiKeyAuthenticationManager(String apikey) {
		this.apikey = apikey;
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		String principal = (String) authentication.getPrincipal();

		if (!apikey.equals(principal)) {
			throw new BadCredentialsException("Invalid Api Key");
		}

		authentication.setAuthenticated(true);

		return authentication;
	}

}
