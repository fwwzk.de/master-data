package de.higger.freewartoolset.master_data.domain;

import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.NonNull;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Npc {

	@NonNull
	private String link;

	@NonNull
	private String bezeichnung;

	@NonNull
	private NpcArt npcArt;

	private Optional<Range> lebenspunkte = Optional.empty();

	private Optional<Range> angriffsstaerke = Optional.empty();

	private Optional<Range> erfahrung = Optional.empty();

	private Optional<Range> gold = Optional.empty();
}
