package de.higger.freewartoolset.master_data.domain;

public enum Rasse {
	DUNKLE_MAGIER("dunkler Magier"), MENSCH_ARBEITER("Mensch/Arbeiter"), MENSCH_KAEMPER("Mensch/Kämpfer"),
	MENSCH_ZAUBERER("Mensch/Zauberer"), NATLA_HAENDLER("Natla-Händler"), ONLO("Onlo"), KEURONER("Keuroner"),
	SERUM_GEIST("Serum-Geist"), TARUNER("Taruner");

	private String bezeichnung;

	Rasse(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}
}
