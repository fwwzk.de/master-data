package de.higger.freewartoolset.master_data.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.NonNull;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Range {
	
	public static final Range EMPTY = new Range(0, 0);

	@NonNull
	private Integer unteresLimit;

	@NonNull
	private Integer oberesLimit;

}
